#!/bin/bash

# Check for the Big Sur location first
big_sur_dnd_prefs=$(plutil -extract dnd_prefs xml1 -o - ~/Library/Preferences/com.apple.ncprefs.plist)

# For earlier MacOS, the dnd_prefs is not present in com.apple.ncprefs.plist
if [[ "$big_sur_dnd_prefs" =~ "invalid key path: dnd_prefs" ]]
then
    echo "Using legacy prefs" 1>&2
    dnd_enabled=$(defaults -currentHost read "${HOME}/Library/Preferences/ByHost/com.apple.notificationcenterui" doNotDisturb)
else
    echo "Using Big Sur prefs" 1>&2
    # Thanks go to paulw (https://www.alfredforum.com/profile/6376-paulw/) on the Alfred forum for coming up with this snippet
    dnd_enabled=$(echo "$big_sur_dnd_prefs" \
                  | xpath -q -e 'string(//data)' \
                  | base64 -D \
                  | plutil -convert xml1 - -o - \
                  | xpath -q -e 'boolean(//key[text()="userPref"]/following-sibling::dict/key[text()="enabled"])' )
fi

if [ "$dnd_enabled" = 0 ]
then
  echo -n "disabled"
else
  echo -n "enabled"
fi
