
# Do Not Disturb, Limited

Toggle macOS's Do Not Disturb configuration, optionally for a limited time:

![turn dnd on](./doc/dnd-on-tiny.png)

![turn dnd on for 2 minutes](./doc/dnd-on-delay-tiny.png)

![turn dnd off](./doc/dnd-off-tiny.png)

I've chosen to use the keyboard shortcut method when controlling the Do Not
Disturb state, which requires some [setup](#setup). If you care, you can 
[read about why](#why-i-made-this).

[Download Releases](https://gitlab.com/troycurtisjr/do-not-disturb-limited/-/releases)

# Setup

This workflow uses a keyboard shortcut to toggle the Do Not Disturb mode. This means
it does require a small setup to work.

Simply configure the `Turn Do Not Disturb On/Off` keyboard shortcut to match the
one found in the `Key Combo` element in the workflow. By default, this workflow uses
*Ctrl+Option+Shift+Cmd+D*, mostly to just stay out of the way, so if you configure macOS
like shown below, it should Just Work.

![keyboard shortcuts](./doc/keyboard-shortcuts-tiny.png)

```
NOTE: With Alfred v3.8.1 on macOS 10.14.4 I could not use any Function keys
successfully for the key combo. It worked when I manually typed it, but I could
never get it injected by Alfred successfully.
```

# Why I Made This

Firstly, because Alfred is becoming essential for my work on macOS. :smile: 

I started by using Vitor Galvao's great
[CalmNotifications](https://github.com/vitorgalvao/alfred-workflows/tree/master/CalmNotifications)
workflow. While it did work, I'm very forgetful and after turning Do Not Disturb
on for a meeting, I'd forget to turn it back off! So I started looking into ways
to automatically disable it automatically. [Muzzle App](https://muzzleapp.com/)
was a pretty neat idea, but I already have enough apps running so I didn't want
to add yet another one. So I turned to Alfred! And nicely enough there is delay
capability built right in.

The other thing I wanted to fix was the Do Not Disturb indication. Using the
typical "set a value in a plist file and kill NotificationCenter" would usually
result in it always appearing as though Do Not Disturb was on. So I settled on
using a key combo to toggle DND, though I still reference the plist file to determine
the current DND state.

# What This Does and Does Not Do

## What it Does

If you do not specify a time (in minutes), the default toggle off at midnight
will happen. If you specify a delay time, then it will be toggled back to the
other state when the timer expires. Note that it will not blindly toggle the
state, but rather it checks to see if the current state is in the desired state,
and if not, issues the toggle.

## What is Does NOT Do

This is a very simple workflow. The delay is *NOT* massively robust. It is using
Alfred's built-in delay utility. If Alfred or your mac restarts, DND will *NOT*
get toggled back. I could use a more complicated mechanism like [EggTimer
V2](https://www.alfredforum.com/topic/275-eggtimer-v2-updated-to-20-final/) does
to make it resilient to a few different failure modes, but for my usage, the simple
Alfred delay works well enough, at least for now.

# License

I've licensed the "code" in the workflow under
[MIT](https://opensource.org/licenses/MIT) and the icon was created by [Pixel
Perfect](https://www.flaticon.com/authors/pixel-perfect ) and used under [CC By
3.0](https://creativecommons.org/licenses/by/3.0/).

# Acknowledgments

[paulw](https://www.alfredforum.com/profile/6376-paulw/): Shell snippet for Big Sur compatibility.
