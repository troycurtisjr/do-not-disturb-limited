#!/bin/bash

NAMESPACE=troycurtisjr
REPO=do-not-disturb-limited

VER_TAG="$1"

if [ -z "$VER_TAG" ]
then
    echo "usage: $0 <version tag>"
    echo "usage: $0 <version tag> delete"

    exit 1
fi

if [ -z "$GITLAB_API_TOKEN" ]
then
    echo "GITLAB_API_TOKEN must be set to the API token to use"
    exit 1
fi

if [ "$2" = "delete" ]
then
    echo "Deleting $VER_TAG"
    exec curl --request DELETE --header "PRIVATE-TOKEN: $GITLAB_API_TOKEN" https://gitlab.com/api/v4/projects/${NAMESPACE}%2F${REPO}/releases/$VER_TAG
fi

echo "Posting the release for $VER_TAG"

cat <<EOF | curl -sS --request POST --header "PRIVATE-TOKEN: $GITLAB_API_TOKEN" --header 'Content-Type: application/json' --data @- https://gitlab.com/api/v4/projects/${NAMESPACE}%2F${REPO}/releases | jq
{
  "name" : "Do Not Disturb, Limited $VER_TAG",
  "tag_name": "$VER_TAG",
  "description": "This is the $VER_TAG release",
  "assets" :
  {
    "links": [
      {
        "name": "$VER_TAG Alfred Workflow",
        "url": "https://gitlab.com/${NAMESPACE}/${REPO}/-/jobs/artifacts/${VER_TAG}/raw/do_not_disturb_limited_${VER_TAG}.alfredworkflow?job=package"
      }
    ]
  }
}
EOF

echo
