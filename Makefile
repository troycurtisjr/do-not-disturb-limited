
# Pull the version from the CI system if set
CI_COMMIT_TAG?=DEV
VERSION?=${CI_COMMIT_TAG}
ARCHIVE_NAME:="do_not_disturb_limited_${VERSION}.alfredworkflow"

SOURCE_DIR:=source/
BUILD_DIR:=buildoutput/
SOURCE_FILES:=$(wildcard ${SOURCE_DIR}*)
BUILD_FILES:=${SOURCE_FILES:${SOURCE_DIR}%=${BUILD_DIR}%}

.PHONY: all version package clean build

ifndef VERBOSE
.SILENT:
endif

all:

version:
	@echo ${VERSION}
	@echo $(BUILD_FILES)
	@echo $(SOURCE_FILES)

package: ${ARCHIVE_NAME}

build: ${BUILD_FILES}

clean:
	rm -f *.alfredworkflow
	rm -fr ${BUILD_DIR}

$(BUILD_DIR):
	mkdir -p $@

$(BUILD_FILES): $(BUILD_DIR)

$(BUILD_FILES): $(BUILD_DIR)%: $(SOURCE_DIR)%
	if [ $* = "info.plist" ]; then echo "$< -> $@"; sed 's/DEVVER/${VERSION}/' $< > $@; \
  else cp -v $< $@; fi

$(ARCHIVE_NAME): $(BUILD_FILES)
	rm -f $@
	cd ${BUILD_DIR} && zip ../$@ $(notdir $^)
